#
# Learning and using the os module.
#

import os
from os import path


def main():

    print os.uname()

    cwd = os.getcwd()
    files_dir = 'files'
    files_dir_exists = os.path.exists(cwd + '/' + files_dir)


    # Make a directory.
    if not files_dir_exists:
        os.mkdir('files', 0755)
        print 'Created files directory..'
    else:
        print 'Files directory already exists.'


if __name__ == '__main__':
    main()